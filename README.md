# CarCar
-----------------------------**

Team:

* Will C. - Sales microservice
* Sal H. - Service microservice

## Design
This application is a website were the user can keep track of and create inventory of vehicle, sales of vehicle, and service appponitments.
![Context Map](https://files.slack.com/files-pri/T03A1FANDTQ-F0435451PTK/betaproject.png "Diagram" )
## Service microservice
Service was to make a technician, create a list of Appts, create a list of service history.
be able to "cancel" an appointment or mark it "finished"
in the service we display the vin, name, time and date of appointment

** WHEN STARTING APP, CREATE A SUPER USER FOR LOCALHOST:8080/ADMIN (PYTHON MANAGE.PY CREATESUPERUSER) ONCE IN THE ADMIN PORTAL GO TO STATUSES, THEN CREATE 3 STATUSES (1.Scheduled 2.Finished 3.Canceled) THIS IS NEEDED TO SET THE STATUS OF THE APPOINTMENT. **


## Sales microservice
The Sales microservice contains 4 aggregates, Sales Person, AutomobileVo(which polls data from the inventory Automobile), and Sell a Car
There is 5 components related to those aggregates
Make a customer
Make a Sales person
Make a sale
    - Will remove a vin from the drop down menu once the car is sold
Sales person history
    - Contains a drop down menu that when sales person is selected will show all that persons sales history
Sales List
    - shows a list of all the sales that have been made

 ## Inventory
Inventory contains 6 components related 
Make a Automobile
Automobile List
Make a manufacturer
manufacturer list
Make a Model 
Model List

## How to start application
-MUST HAVE DOCKER TO RUN
1. fork project in repository
2. in terminal mkdir <name of directory you want project in>
3. cd <name of directory>
4. git clone <copy https Link of forked project>
5. cd <directory name of project>
6. run code .
7. docker volume create beta-data
8. docker-compose build
9. docker-compose up
10. refer to instructions in service section to create statuses
11. open http://localhost:3000/ in browser 
12. you can now make your inventory for vehicles
13. you can sell cars and make appointments to service cars



