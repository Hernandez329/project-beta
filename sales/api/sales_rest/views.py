from sqlite3 import Cursor
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import render
import json
from django.forms.models import model_to_dict
from common.json import ModelEncoder
from .models import (
    AutomobileVO,
    Customer,
    SalesPerson,
    Customer,
    SellCar,
)


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "model_name",
        "id"
    ]



class SalesPeopleListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_num",
        "id"
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "phone_number",
        "address",
        "id"
    ]

class SellCarEncoder(ModelEncoder):
    model = SellCar
    properties= [
        "car",
        "sales_person",
        "customer",
        "price",
        "id",
        "sold",
    ]
    encoders ={
        "sales_person": SalesPeopleListEncoder(), 
        "customer": CustomerListEncoder(),
        "car": AutomobileVOEncoder(), 
    }


@require_http_methods(["GET", "POST"])
def sales_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPeopleListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        sales_people = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_people,
            encoder=SalesPeopleListEncoder,
            safe=False
        )



@require_http_methods(["GET"])
def cars(request):
    if request.method == "GET":
        cars = AutomobileVO.objects.all()
        return JsonResponse(
            {"cars": cars},
            encoder=AutomobileVOEncoder
        )


@require_http_methods(["GET", "POST"])
def customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
            customers,
            encoder=CustomerListEncoder,
            safe=False
        )




@require_http_methods(["GET", "POST"])
def sold_cars(request):
    if request.method == "GET":
        sales = SellCar.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder= SellCarEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            car= AutomobileVO.objects.get(vin=content["car"])
            content["car"] = car
        
            sales_person = SalesPerson.objects.get(employee_num=content["sales_person"])
            content["sales_person"] = sales_person

            customer= Customer.objects.get(id=content["customer"])
            content["customer"] = customer


        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid  id"},
                status=400,
            )
        car = SellCar.objects.create(**content)
        return JsonResponse(
            car,
            encoder=SellCarEncoder,
            safe=False
        )
 
@require_http_methods(["GET"])
def sale_detail(request, pk):
    if request.method == "GET":
        sale=SellCar.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder= SellCarEncoder,
            safe= False,
        )
    



