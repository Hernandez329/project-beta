from django.db import models
from django.core.validators import MaxValueValidator
from django.core.validators import RegexValidator
# Create your models here.





class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)
    model_name = models.CharField(max_length=100)

    def __str__(self):
        return self.model_name

class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_num =  models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=50)
    phone_number = models.CharField(unique=True, max_length=10, validators=[RegexValidator(r"^\+?1?\d{8,15}$")])

    def __str__(self):
        return self.name

    

class SellCar(models.Model):
    car = models.ForeignKey(AutomobileVO, related_name="car", on_delete=models.PROTECT)
    sales_person = models.ForeignKey(SalesPerson, related_name="vendor", on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, related_name="customer", on_delete=models.PROTECT)
    price = models.CharField(max_length=10)
    sold = models.BooleanField(default=True)


    # def __str__(self):
    #     return self.car







   

    




