# Generated by Django 4.0.3 on 2022-09-13 19:10

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0005_remove_sellcar_status_address_city_address_country_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='zip_code',
            field=models.IntegerField(validators=[django.core.validators.MaxValueValidator(6)]),
        ),
    ]
