from django.urls import path

from .views import (
    sales_people,
    customers,
    sold_cars,
    cars,
    sale_detail
)






urlpatterns = [
    path("sales/person/", sales_people, name="sales_people"),
    path("sales/customers/", customers, name="customers"),
    path("cars/sold/", sold_cars, name="sold_cars"),
    path("cars/", cars, name="cars"),
     path("cars/sold/<int:pk>/", sale_detail, name="sale_detail"),
]