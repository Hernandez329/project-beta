import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="cars/sold">Sold Cars</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="customer/form">Customer Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="salesperson/form">Sales Person Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="carsale/form">Car Sale Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="models">Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="model/form">Model Form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="cars/sold/history">Sales People History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments/">Appointment List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments/new/">New Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="technician/new/">Enter a Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="automobiles/">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="manufacturers/">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="manufacturers/new/">Create a Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="automobiles/new/">Create a Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="history/">Service History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
