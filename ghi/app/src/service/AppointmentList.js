import React from "react";

class AppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cars: [],
            appointments: [],
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleFinished = this.handleFinished.bind(this);
    }


    async componentDidMount() {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState(({ appointments: data.appointments }))
            console.log(data.appointments)
        }

        const carUrl = "http://localhost:8080/api/cars/";
        const carResponse = await fetch(carUrl);
        if (carResponse.ok) {
            const carData = await carResponse.json();
            this.setState({ cars: carData.cars })
            // console.log(carData.cars)
        }
    }

    async handleFinished(id) {
        const url = `http://localhost:8080/api/appointments/${id}/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ status: 2 }),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            window.location.reload()
        }

    }


    async handleDelete(id) {
        fetch(`http://localhost:8080/api/appointments/${id}`, {
            method: "DELETE"
        }).then((result) => {
            result.json().then((resp) => {
                console.warn(resp)
                window.location.reload()
            })
        })
    }


    render() {
        return (
            <>
                <div>
                    <h1>Service Appointments</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>VIP</th>
                                <th>VIN</th>
                                <th>Customer Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.appointments.map(appointment => {
                                if (appointment.status === "Scheduled") {
                                    return (
                                        <tr key={appointment.id}>
                                            <td>{(appointment.vin ? "Yes" : "No")}</td>
                                            <td>{appointment.vin}</td>
                                            <td>{appointment.customer_name}</td>
                                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                                            <td>{new Date(appointment.date_time).toLocaleTimeString('en-US', { timeZone: "UTC", hour: "2-digit", minute: "2-digit" })}</td>
                                            <td>{appointment.technician.name}</td>
                                            <td>{appointment.reason}</td>
                                            <td>
                                                <button onClick={() => this.handleDelete(appointment.id)} type="button" className="btn btn-danger">Cancel</button>
                                                <button onClick={() => this.handleFinished(appointment.id)} type="button" className="btn btn-success">Finished</button>
                                            </td>
                                        </tr>
                                    )
                                    
                                    
                                }
                            })}
                        </tbody>
                    </table>
                </div>
            </>
        )
    }


}

export default AppointmentList;