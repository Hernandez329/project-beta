import React from "react";

class ServiceHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            search: "",
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    // async componentDidMount() {
    //     const url = "http://localhost:8080/api/appointments/";
    //     const response = await fetch(url);
    //     if (response.ok) {
    //         const data = await response.json();
    //         this.setState(({appointments: data.appointments}))
    //         console.log(data.appointments)
    //     }
    // }
    async handleSubmit(event) {
        event.preventDefault()
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            // this.setState(({appointments: data.appointments}))
            console.log(data.appointments)
        
        const test = data.appointments.filter((appointment) => {
            return appointment.vin.vin === this.state.search
        })
       this.setState({appointments: test})
    }
    }


    handleChange(event) {
        const value = event.target.value;
        this.setState({search: value})
    }

    render() {
        return (
            <>
                <div>
                <form id="search-by-vin-form">
                    <div className="input-group mb-3 mt-5">
                        <input value={this.state.search} onChange={this.handleChange} 
                        type="text" className="form-control" 
                        placeholder="Enter VIN to search" id="search" name="search" />
                            <button onClick={this.handleSubmit} className="input-group-text">Search by VIN</button>
                    </div>
                </form>   

                <h1>Service Appointments</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            {/* if (appointment.vin.vin === this.state.search) { */}

                            
                            return (
                                <tr key={appointment.id}>
                                
                                <td>{appointment.vin.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                                <td>{new Date(appointment.date_time).toLocaleTimeString('en-US', {timeZone: "UTC", hour: "2-digit", minute: "2-digit"})}</td>
                                <td>{appointment.technician.name}</td>
                                <td>{appointment.reason}</td>
                                
                                </tr>
                            )
                            {/* } */}
                        })}
                    </tbody>
                </table>
                </div>
            </>
        )
    }
}

export default ServiceHistory;