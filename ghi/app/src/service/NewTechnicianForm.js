import React from "react";

class NewTechnicianForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employee_number: '',
            
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleTechnicianNameChange = this.handleTechnicianNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
    }
    // async componentDidMount() {
    //     const url = 'http://localhost:8080/api/technicians';
    //     const response = await fetch(url);

    //     if (response.ok) {
    //         const data = await response.json();
    //         this.setState({technicians: data.technicians});
    //     }
    // }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        // delete data.technicians;
        console.log(data)

        const techsUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'applications/json'
            }
        };
        const response = await fetch(techsUrl, fetchConfig)
        if (response.ok) {
            const newTech = await response.json();
            console.log(newTech)
            this.setState({
                name: '',
                employee_number: '',
            });
        }
    }

    handleTechnicianNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    }

    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({employee_number: value});
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Enter a Technician</h1>
                    <form onSubmit={this.handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleTechnicianNameChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Tehcnician Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleEmployeeNumberChange} value={this.state.employee_number} placeholder="employee_number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>

        )
    }
}

export { NewTechnicianForm };