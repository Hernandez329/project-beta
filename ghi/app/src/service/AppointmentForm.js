import React from "react";

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: '',
            customer_name: '',
            date_time: "",
            technician: '',
            technicians: [],
            reason: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians })
            console.log(data)
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.technicians

        const appUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'applications/json'
            }
        };
        const response = await fetch(appUrl, fetchConfig)
        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment)
            this.setState({
                vin: '',
                customer_name: '',
                date_time: "",
                technician: '',
                reason: '',

            })
        }
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin: value});
    }

    handleCustomerNameChange(event) {
        const value = event.target.value;
        this.setState({customer_name: value});
    }

    handleDateChange(event) {
        const value = event.target.value;
        this.setState({date_time: value});
    }

    handleTimeChange(event) {
        const value = event.target.value;
        this.setState({date_time: value});
    }

    handleTechnicianChange(event) {
        const value = event.target.value;
        this.setState({technician: value});
    }

    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({reason: value});
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add Service Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleVinChange} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleCustomerNameChange} value={this.state.customer_name} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" />
                                <label htmlFor="customer">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleDateChange} value={this.state.date_time} placeholder="date" required type="datetime-local" name="date" id="date" className="form-control" />
                                <label htmlFor="date">Date & Time</label>
                            </div>
                            {/* <div className="form-floating mb-3">
                                <input onChange={this.handleTimeChange} value={this.state.date_time} placeholder="time" required type="time" name="time" id="time" className="form-control" />
                                <label htmlFor="time">Time</label>
                            </div> */}
                            <div className="form-floating mb-3">
                                <select onChange={this.handleTechnicianChange} value={this.state.technician} placeholder="technician" required type="text" name="technician" id="technician" className="form-control" >
                                <option value="">Choose a Tech</option>
                                {this.state.technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>
                                        {technician.name}
                                        </option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>

        )
    }
}

export default AppointmentForm;