import React from 'react';

class ModelForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            pictureUrl: '',
            manufacturers: []
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event){
        const value = event.target.value
        this.setState({name: value})
    }
    handleManufacturerChange(event){
        const value = event.target.value
        this.setState({manufacturer_id: value})
    }
    handleColorChange(event){
        const value = event.target.value
        this.setState({color: value})
    }
    handlePictureUrlChange(event){
        const value = event.target.value
        this.setState({pictureUrl: value})
    }
    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state}
        data.picture_url = data.pictureUrl
        delete data.pictureUrl
        delete data.manufacturers

        const url = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok){
            const newModel = await response.json()
            console.log(newModel)

            const clear = {
                name: '',
                manufacturer_id: '',
                pictureUrl: '',
            }
            this.setState(clear)
        }


    }
    
    async componentDidMount(){
        const url = "http://localhost:8100/api/manufacturers/"
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json()
            this.setState({manufacturers: data.manufacturers})
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name}
                                    onChange={this.handleNameChange} placeholder="name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.pictureUrl}
                                    onChange={this.handlePictureUrlChange} placeholder="pic" type="text" name="pic" id="pic" className="form-control" />
                                <label htmlFor="pic">Car Picture</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.manufacturer_id} onChange={this.handleManufacturerChange}
                                    required name="manufacturer" id="manufacturer" className="form-select">
                                    <option value="">Manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default ModelForm;