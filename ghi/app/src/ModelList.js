import React from 'react';

class ModelList extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = { models: [] }
    }
    async componentDidMount() {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url);
        if (response.ok);
        const data = await response.json();
        console.log(data)
        this.setState({ models: data.models })
    }
    render() {
        return (
            <div className="container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Model Name</th>
                            <th>Manufacturer</th>
                            <th>Car Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => {
                            return (
                                <tr key={model.id}>
                                    <td>
                                        {model.name}
                                    </td>
                                    <td>{model.manufacturer.name}</td>
                                    <td>
                                        <img src={model.picture_url} />
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ModelList