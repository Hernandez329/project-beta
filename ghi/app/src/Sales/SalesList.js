import React from 'react';

class SalesList extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = {
            sales: []
        }
    }
    async componentDidMount() {
        const carsUrl = "http://localhost:8090/api/cars/sold/"
        const response = await fetch(carsUrl);
        if (response.ok){
            const carData = await response.json();
            console.log(carData)
            this.setState({ sales: carData.sales })
        }
    }
    render() {
        return (
            <div className="container">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Sales Person</th>
                                <th>Employee Number</th>
                                <th>Customer</th>
                                <th>Vin</th>
                                <th>Sale Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.sales.map(sale => {
                                return (
                                    <tr key={sale.id}>
                                        <td>{sale.sales_person.name}</td>
                                        <td>{sale.sales_person.employee_num}</td>
                                        <td>{sale.customer.name}</td>
                                        <td>{sale.car.vin}</td>
                                        <td>{sale.price}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>

                )
    }
}

                export default SalesList