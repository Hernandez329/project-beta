import React from 'react';


class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            employeeNum: '',
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumChangle = this.handleEmployeeNumChangle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value
        this.setState({ name: value })
    }
    handleEmployeeNumChangle(event) {
        const value = event.target.value
        this.setState({ employeeNum: value })
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.employee_num = data.employeeNum
        delete data.employeeNum

        const url = "http://localhost:8090/api/sales/person/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)
        console.log(response)
        if (response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson)

            const clear = {
                name: '',
                employeeNum: '',
            }
            this.setState(clear)
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Sales Person</h1>
                        <form onSubmit={this.handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name}
                                    onChange={this.handleNameChange} placeholder="name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.employeeNum}
                                    onChange={this.handleEmployeeNumChangle} required type="text" id="E#" name="E#" className="form-control" />
                                <label htmlFor="E#">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default SalesPersonForm