from django.db import models
from django.urls import reverse

# Create your models here.
class Status(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=12, unique=True)
    
    def __str__(self):
        return self.name

    class Meta:
        ordering =('id',)
        verbose_name_plural = "statuses"

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
      


class Technician(models.Model):
    name = models.CharField(max_length=45)
    employee_number = models.SmallIntegerField(unique=True)

    def __str__(self):
        return self.name


class ServiceAppointment(models.Model):
    customer_name = models.CharField(max_length=55)
    date_time = models.DateTimeField()
    vin = models.CharField(max_length=17)
    technician = models.ForeignKey(Technician, related_name="appointments", on_delete=models.CASCADE)
    reason = models.TextField()
    status = models.ForeignKey(Status, related_name="appointments", on_delete=models.PROTECT)

    def __str__(self):
        return self.customer_name

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="Scheduled")
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

    def finished(self):
        status = Status.objects.get(name="Finished")
        self.status = status
        self.save

    def cancel(self):
        status = Status.objects.get(name="Canceled")
        self.status = status
        self.save()
    
    def get_api_url(self):
        return reverse("api_list_appointment", kwargs={"pk": self.id})

