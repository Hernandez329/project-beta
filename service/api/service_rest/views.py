from re import S
from django.shortcuts import render
from common.json import ModelEncoder
from .models import ServiceAppointment, Technician, AutomobileVO, Status
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

# Create your views here.


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "id"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number", "id"]


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = ["customer_name", "date_time",
                  "reason", "vin", "technician", "status", "id"]

    encoders = {
        "technician": TechnicianEncoder(),
        # "status": StatusEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        
        content = json.loads(request.body)

        try:

            technician = Technician.objects.get(id=content['technician'])
            content['technician'] = technician
            
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {'message': 'failed'},
                status=400,
            )

        appointment = ServiceAppointment.create(**content)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_list_appointment(request, pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        ServiceAppointment.objects.filter(id=pk).update(**content)

        appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )

    else:
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    



@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {'technicians': technicians},
            encoder=TechnicianEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {'message': "Invalid tech"}
            )


@require_http_methods(["GET"])
def cars(request):
    if request.method == "GET":
        cars = AutomobileVO.objects.all()
        return JsonResponse(
            {"cars": cars},
            encoder=AutomobileVOEncoder
        )





