from django.contrib import admin

from .models import Status, AutomobileVO, Technician, ServiceAppointment
# Register your models here.

class StatusAdmin(admin.ModelAdmin):
    pass 

class AutomobileVOAdmin(admin.ModelAdmin):
    pass

class TechnicianAdmin(admin.ModelAdmin):
    pass

class ServiceAppointmentAdmin(admin.ModelAdmin):
    pass

admin.site.register(ServiceAppointment, ServiceAppointmentAdmin)
admin.site.register(Technician, TechnicianAdmin)
admin.site.register(AutomobileVO, AutomobileVOAdmin)
admin.site.register(Status, StatusAdmin)